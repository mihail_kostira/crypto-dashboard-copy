package net.bespokeservices.jerseyproviders;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import javax.ws.rs.Produces; 
import java.text.SimpleDateFormat;
/**
 * Enables jackson to (de)serialize jodatime DateTime
 * @author Iv
 *
 */
@Provider
@Produces("application/json")
public class JacksonConfigurator implements ContextResolver<ObjectMapper> {

    private ObjectMapper mapper;

    public JacksonConfigurator() {
    	JacksonJsonProvider jacksonJsonProvider = new JacksonJaxbJsonProvider();
		mapper = new JodaMapper();
		jacksonJsonProvider.setMapper(mapper);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Override
    public ObjectMapper getContext(Class<?> arg0) {
        return mapper;
    }

}
