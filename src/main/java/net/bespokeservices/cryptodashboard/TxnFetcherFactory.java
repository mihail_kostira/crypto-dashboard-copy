package net.bespokeservices.cryptodashboard;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class TxnFetcherFactory {
	@Deprecated
	private int blocktrailRestApiRefreshSeconds;
	// private ScheduledExecutorService executorService;

	public TxnFetcherFactory(int blocktrailRestApiRefreshSeconds) throws SchedulerException {
		super();
		this.blocktrailRestApiRefreshSeconds = blocktrailRestApiRefreshSeconds;

		// executorService = new ScheduledThreadPoolExecutor(3);
	}

	public TxnHistoryFetcher getFetcher(CryptoAddress address) {

		switch (address.getCoinType()) {
		case "btc":
			return new BlocktrailTxnHistoryFetcher();
		case "bch":
			return new BlocktrailTxnHistoryFetcher();
		case "eth":
			return new BlockcypherTxnHistoryFetcher();
		case "doge":
			return new BlockcypherTxnHistoryFetcher();
		case "ltc":
			return new BlockcypherTxnHistoryFetcher();
		case "dash":
			return new BlockcypherTxnHistoryFetcher();
		}

		// return null;
		throw new UnsupportedOperationException("Unsupported coin type: " + address.getCoinType());
	}
	/*
	 * public TxnTracker createTracker(CryptoAddress address) { final
	 * AbstractRESTTxnTracker tracker = new BlocktrailRESTTxnTracker(address);
	 * ScheduledFuture<?> scheduledFuture = executorService.scheduleAtFixedRate(new
	 * Runnable() {
	 * 
	 * @Override public void run() { tracker.update(); } }, 0,
	 * blocktrailRestApiRefreshSeconds, TimeUnit.SECONDS);
	 * 
	 * tracker.setScheduledFuture(scheduledFuture); return tracker; }
	 */
}
