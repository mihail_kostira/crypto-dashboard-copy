package net.bespokeservices.cryptodashboard;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

import net.bespokeservices.cryptodashboard.server.EmbeddedServer;
import net.bespokeservices.cryptodashboard.server.resources.OrderStatusResource;

/**
 * 
 * Use these VM args to connect through a proxy (e.g. Fiddler, to intercept and
 * inspect requests) -DproxySet=true -DproxyHost=127.0.0.1 -DproxyPort=8888
 * -Djavax.net.ssl.trustStore="C:\dev\keys\FiddlerKeystore"
 * -Djavax.net.ssl.trustStorePassword=<password>
 */
public class App {
	private static final Logger logger = Logger.getLogger(App.class.getName());

	/**
	 * @param args
	 *            A single argument, the path to the config file
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// set some proxy settings in case we want to use Fiddler to intercept requests
		System.setProperty("http.proxyHost", "127.0.0.1");
		System.setProperty("http.proxyPort", "8888");
		// for capturing HTTPS traffic
		System.setProperty("https.proxyHost", "127.0.0.1");
		System.setProperty("https.proxyPort", "8888");

		System.out.println("Command-line arguments:");
		for (String arg : args) {
			System.out.println(arg);
		}

		// Parse the config file path arg
		Options options = new Options();
		options.addOption("cfg", true, "config file, defaults to resources-external/cryptodashboard.cfg");
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd;
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e1) {
			System.err.println("Error parsing command line arguments");
			new HelpFormatter().printHelp("Start the service with config settings from the specified file", options);
			;
			e1.printStackTrace();
			return;
		}

		// Load config file as props
		Properties config = new Properties();
		String configFilePath = cmd.getOptionValue("cfg", "resources-external/cryptodashboard.cfg");
		try {
			InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFilePath);
			config.load(stream);
		} catch (IOException e1) {
			System.err.println("Error loading config file from" + configFilePath);
			throw (e1);
		}

		// configure logging
		// route all JUL log records to SLF4J
		SLF4JBridgeHandler.install();
		URL url = Thread.currentThread().getContextClassLoader().getResource(config.getProperty("log4jConfigFile"));
		org.apache.log4j.PropertyConfigurator.configure(url.getPath());

		// start the server
		new CryptoDasboardBackend(config).start();
	}
}
