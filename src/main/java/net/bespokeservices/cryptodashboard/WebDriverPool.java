package net.bespokeservices.cryptodashboard;

import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

@Component
public class WebDriverPool extends GenericObjectPool<WebDriver> implements ObjectPool<WebDriver>	 {

	private int maxWebDrivers;
	
	public WebDriverPool() {
		this(5);
	}

	public WebDriverPool(int maxWebDrivers) {
		this(new PooledWebDriverFactory(), maxWebDrivers);
		
	}

	public WebDriverPool(PooledObjectFactory<WebDriver> factory, int maxWebDrivers) {
		super(factory);
		
		GenericObjectPoolConfig config = new GenericObjectPoolConfig();
		config.setMaxTotal(maxWebDrivers);
		setConfig(config);
		
	}
	
	

}
