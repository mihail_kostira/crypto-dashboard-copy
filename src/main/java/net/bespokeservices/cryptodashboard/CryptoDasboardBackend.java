package net.bespokeservices.cryptodashboard;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;

import net.bespokeservices.cryptodashboard.server.EmbeddedServer;

public class CryptoDasboardBackend {
	private static final Logger logger = Logger.getLogger(App.class.getName());

	private ClassPathXmlApplicationContext applicationContext;

	private EmbeddedServer server;

	private Properties config;

	public CryptoDasboardBackend(Properties config) {
		this.config = config;
		// TODO Auto-generated constructor stub
		logger.error("starting crypto-dahsboard with config: \n" + config.toString());
		System.setProperty("webdriver.chrome.driver", config.getProperty("webdriver.chrome.driver"));

		applicationContext = new ClassPathXmlApplicationContext(new String[] { "resources-external/spring-config.xml" },
				false);

		PropertiesPropertySource configFilePropsSource = new PropertiesPropertySource("configFilePropsSource", config);
		applicationContext.getEnvironment().getPropertySources().addLast(configFilePropsSource);

		applicationContext.refresh();

		((AbstractApplicationContext) applicationContext).registerShutdownHook();
	}

	/**
	 * Starts the server in the current thread
	 */
	public void start() {
		server = (EmbeddedServer) applicationContext.getBean("server");
		try {
			server.start();
		} catch (IOException | URISyntaxException e) {
			System.err.println("Error starting server");
			e.printStackTrace();
		}
	}

	public void stop() throws Exception {
		server.stop();
	}

	public Properties getConfig() {
		return config;
	}

	public boolean isStarted() {
		return server != null && server.isStarted();
	}
	
	
}
