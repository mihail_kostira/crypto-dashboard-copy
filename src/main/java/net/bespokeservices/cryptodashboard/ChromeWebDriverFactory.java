package net.bespokeservices.cryptodashboard;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;

public class ChromeWebDriverFactory {
	private static final Logger logger = Logger.getLogger(ChromeWebDriverFactory.class);
	
	public List<WebDriver> drivers;
	public volatile boolean shutdownInProgress;
		
	public ChromeWebDriverFactory() {
		super();
		shutdownInProgress = false;
		drivers = new ArrayList<WebDriver>();
		
		// TODO Auto-generated constructor stub
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() { 
				shutdownInProgress = true;
				logger.error("Shutdown Hook is running !");   
				for (WebDriver driver : drivers) {
					logger.error("calling quit on driver " + driver);   
					driver.quit();
				}
			}
		});
	}

	public WebDriver createWebDriver() {
		if (shutdownInProgress) {
			return null;
		}
		
		WebDriver driver;
		ChromeOptions chromeOptions = new ChromeOptions();

		chromeOptions.setHeadless(true);

		// use proxy to block requests to e.g. adsense, fb and save on bandwith
		boolean useProxy = false;
		if (useProxy) {
			BrowserMobProxy proxyServer = new BrowserMobProxyServer();
			List<String> allowUrlPatterns = new ArrayList<String>();
			allowUrlPatterns.add("https?://.*(remixshop.com)+.*");
			proxyServer.whitelistRequests(allowUrlPatterns, 404);
			try {
				proxyServer.start(9091, InetAddress.getLocalHost());
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Proxy proxy = ClientUtil.createSeleniumProxy(proxyServer);
			chromeOptions.setCapability("proxy", proxy);

		}

		// Don't load images
		HashMap<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.managed_default_content_settings.images", 2);
		chromeOptions.setExperimentalOption("prefs", prefs);

		driver = new ChromeDriver(chromeOptions);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		drivers.add(driver);

		return driver;
	}
}
