package net.bespokeservices.cryptodashboard;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.openqa.selenium.WebDriver;

public class PooledWebDriverFactory 
extends BasePooledObjectFactory<WebDriver> 
implements PooledObjectFactory<WebDriver> {

	private ChromeWebDriverFactory factory;
	
	public PooledWebDriverFactory() {
		factory = new ChromeWebDriverFactory(); 
	}
	
	@Override
	public WebDriver create() throws Exception {
		return factory.createWebDriver();
	}

	@Override
	public PooledObject<WebDriver> wrap(WebDriver arg0) {
		return new DefaultPooledObject<WebDriver>(arg0);
	}
}
