package net.bespokeservices.cryptodashboard;

public class CrawlerParseException extends Exception {

	public CrawlerParseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CrawlerParseException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CrawlerParseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CrawlerParseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CrawlerParseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
