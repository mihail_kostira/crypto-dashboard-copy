package net.bespokeservices.cryptodashboard;

import org.joda.time.DateTime;

public class OrderStatus {
	private String status;
	private DateTime lastUpdated;
	private String updateError;
	
	public OrderStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderStatus(String status, DateTime lastUpdated) {
		super();
		this.status = status;
		this.lastUpdated = lastUpdated;
	}



	public OrderStatus(String status, DateTime lastUpdated, String updateError) {
		super();
		this.status = status;
		this.lastUpdated = lastUpdated;
		this.updateError = updateError;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getUpdateError() {
		return updateError;
	}

	public void setUpdateError(String updateError) {
		this.updateError = updateError;
	}
	
}
