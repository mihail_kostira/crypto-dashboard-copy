package net.bespokeservices.cryptodashboard;

import java.math.BigInteger;
import java.util.Date;

import org.joda.time.DateTime;

public class Txn {
	protected BigInteger value;
	protected int confirmations;
	protected DateTime timeSeen;
	protected DateTime timeConfirmed;
	protected String hash;
	
	public Txn() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Txn(String hash, BigInteger bigInteger, int confirmations, DateTime timeSeen, DateTime timeConfirmed) {
		super();
		this.hash = hash;
		this.value = bigInteger;
		this.confirmations = confirmations;
		this.timeSeen = timeSeen;
		this.timeConfirmed = timeConfirmed;
	}

	public BigInteger getValue() {
		return value;
	}

	public void setValue(BigInteger value) {
		this.value = value;
	}

	public int getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(int confirmations) {
		this.confirmations = confirmations;
	}

	public DateTime getTimeSeen() {
		return timeSeen;
	}

	public void setTimeSeen(DateTime timeSeen) {
		this.timeSeen = timeSeen;
	}

	public DateTime getTimeConfirmed() {
		return timeConfirmed;
	}

	public void setTimeConfirmed(DateTime timeConfirmed) {
		this.timeConfirmed = timeConfirmed;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Override
	public String toString() {
		return "Txn [value=" + value + ", timeSeen=" + timeSeen
				+ ", timeConfirmed=" + timeConfirmed + ", hash=" + hash + "]";
	}
	
	public String toFullString() {
		return "Txn [value=" + value + ", confirmations=" + confirmations + ", timeSeen=" + timeSeen
				+ ", timeConfirmed=" + timeConfirmed + ", hash=" + hash + "]";
	}
	
}
