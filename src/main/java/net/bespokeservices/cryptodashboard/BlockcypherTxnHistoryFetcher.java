package net.bespokeservices.cryptodashboard;

import java.io.IOException;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import net.bespokeservices.cryptodashboard.model.BlockcypherAddressTransactions;
import net.bespokeservices.cryptodashboard.model.BlockcypherTxref;
import net.bespokeservices.cryptodashboard.model.BlocktrailAddressTransactions;
import net.bespokeservices.cryptodashboard.model.BlocktrailTxn;
import net.bespokeservices.cryptodashboard.model.BlocktrailTxnInputOutput;

public class BlockcypherTxnHistoryFetcher implements TxnHistoryFetcher {

	private static final Logger logger = Logger.getLogger(BlockcypherTxnHistoryFetcher.class);

	private int txnPerPageLimit;
	
	
	public BlockcypherTxnHistoryFetcher(int txnPerPageLimit) {
		super();
		this.txnPerPageLimit = txnPerPageLimit;
	}


	public BlockcypherTxnHistoryFetcher() {
		this(50);
	}

	@Override
	public void updateTxnHistory(TxnHistory history, DateTime fromDate, DateTime toDate) {
		logger.debug("updateTxnHistory " + this);
		
		try
		{
			// just start from the very top(newest) block and work down
			int topBlock = -1;
			boolean loadMore = false;

			do {
				BlockcypherAddressTransactions txnsObject;

				txnsObject = loadTxnPage(topBlock, history.getAddress());
				loadMore = processTxns(history, txnsObject, fromDate, toDate);
				topBlock = txnsObject.getBottomBlock();
			} while (loadMore);

			history.setLastUpdated(new DateTime());
			history.setUpdateError(null);
			// TODO history.setValueUnitsPerCoin(100000000);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			history.setUpdateError(e.getMessage());
		}
		
	}


	/**
	 * Adds txns that are in our track time window to the history
	 * 
	 * @param txnsObject
	 * @param fromDate 
	 * @param toDate 
	 * @param history
	 * @return whether to retrieve older pages.False if
	 *  1) the newest txn is before our to-from interval
	 *  or
	 *  2) the oldest txn is already and in our history with >50 confirmations 
	 *  or
	 *  3) there are no more pages
	 */
	private boolean processTxns(TxnHistory history, BlockcypherAddressTransactions txnsObject, DateTime fromDate,
			DateTime toDate) {
		if (txnsObject == null) {
			return false;
		}
		ArrayList<BlockcypherTxref> txns = txnsObject.getTxrefs();
		for (BlockcypherTxref txn : txns) {
			// txns are ordered newest to oldest
			DateTime timeSeen = txn.getReceivedOrConfirmed();
			if (timeSeen.isBefore(fromDate)) {
				// this txn and all older ones are before our track window
				return false;
			}
			if (timeSeen.isAfter(toDate)) {
				// this txn is after our track window, check the earlier ones
				continue;
			}
			
			logger.debug("in the to-from interval");
			// in the to-from interval
			Txn duplicate = addToHistory(history, txn);
			if (duplicate != null 
					&& duplicate.getConfirmations() > 50) {
				logger.debug("in history and confirmed, skip");
				// we already have this txn in history and it has some confirmations
				// so we can be sure earlier txns have been added as well
				return false;
			}
		}

		return txnsObject.isHasMore();
	}

	/**
	 * @param btxn
	 * @param history 
	 * @return true if history has a txn with the same hash and confirmations > 6
	 */
	private Txn addToHistory(TxnHistory history, BlockcypherTxref btxn) {
		logger.debug("addToHistory");
		Txn duplicate = history.getTxn(btxn.getTx_hash());
		
		if (duplicate != null) {
			duplicate.setConfirmations(btxn.getConfirmations());
			return duplicate;
		}
		BigInteger value = btxn.isSpend()? btxn.getValue().multiply(BigInteger.valueOf(-1)) : btxn.getValue() ;
		Txn txn = new Txn(btxn.getTx_hash(), value, btxn.getConfirmations(),
				btxn.getReceived(), btxn.getConfirmed());
		history.addTxn(txn);

		return duplicate;
	}

	/**
	 * @param beforeBlock the newest block to consider, -1 to start from the top block down
	 * @param address
	 * @return
	 * @throws IOException
	 */
	private BlockcypherAddressTransactions loadTxnPage(int beforeBlock, CryptoAddress address) throws IOException {
		logger.debug("loadTxnPage,loading txns beforeBlock " + beforeBlock);
		Client client = ClientProvider.getInstance().getClient();
		Object token = "44681ecce78045d8872cf2ca9701ad68";
		String apiCall;
		if (beforeBlock == -1) {
			// NOTE: using Integer.MAX_VLUE in the 'before' param results in error,
			// so we use this workaround to get the latest block
			String apiNewestBlockCallTemplate = "https://api.blockcypher.com/v1/{0}/main/addrs/{1}?after=0&limit={2}&token={3}";
			apiCall = MessageFormat.format(apiNewestBlockCallTemplate, 
					address.getCoinType(), 
					address.getAddress(), 
					txnPerPageLimit,
					token);
		} else {
			String apiCallTemplate = "https://api.blockcypher.com/v1/{0}/main/addrs/{1}?before={2,number,#}&limit={3}&token={4}";
			apiCall = MessageFormat.format(apiCallTemplate, 
					address.getCoinType(), 
					address.getAddress(), 
					beforeBlock,
					txnPerPageLimit,
					token);
		}
		BlockcypherAddressTransactions txnsObject;
		try {
			logger.debug("retrieving " + apiCall);
			txnsObject = client.target(apiCall).request(MediaType.APPLICATION_JSON)
					.get(BlockcypherAddressTransactions.class);
		} catch (Exception e) {
			logger.error("Error retrieving BlockcypherAddressTransactions object at " + apiCall, e);
			throw new IOException("Error retrieving BlockcypherAddressTransactions object at " + apiCall +" Error is: " + e.getMessage(), e);
		}
		return txnsObject;
	}
	
}
