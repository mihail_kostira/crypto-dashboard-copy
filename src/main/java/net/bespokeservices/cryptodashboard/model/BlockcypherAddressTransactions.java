package net.bespokeservices.cryptodashboard.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.bespokeservices.cryptodashboard.model.BlockcypherTxref;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlockcypherAddressTransactions {
	private String address;
	private long balance;
	private int n_tx;
	private boolean hasMore;
	
	private ArrayList<BlockcypherTxref> txrefs;

	public BlockcypherAddressTransactions() {
		super();
	}
	
	public int getBottomBlock() {
		if (txrefs == null
				|| txrefs.isEmpty()) {
			return -1;
		}
		BlockcypherTxref txref = txrefs.get(txrefs.size()-1);
		return txref.getBlock_height();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}

	public int getN_tx() {
		return n_tx;
	}

	public void setN_tx(int n_tx) {
		this.n_tx = n_tx;
	}

	public boolean isHasMore() {
		return hasMore;
	}

	public void setHasMore(boolean hasMore) {
		this.hasMore = hasMore;
	}

	public ArrayList<BlockcypherTxref> getTxrefs() {
		return txrefs;
	}

	public void setTxrefs(ArrayList<BlockcypherTxref> txrefs) {
		this.txrefs = txrefs;
	}
	 
}
