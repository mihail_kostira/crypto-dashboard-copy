package net.bespokeservices.cryptodashboard.model;

import java.math.BigInteger;
import java.util.ArrayList;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlockcypherTxref {
	protected String tx_hash;
	protected int block_height;
	protected int tx_input_n;
	protected int tx_output_n;
	protected int confirmations;
	protected BigInteger value;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	protected DateTime confirmed;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	protected DateTime received;
		
	public BlockcypherTxref() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BlockcypherTxref(String tx_hash, int block_height, int tx_input_n, int tx_output_n, int confirmations,
			BigInteger value, DateTime confirmed, DateTime received) {
		super();
		this.tx_hash = tx_hash;
		this.block_height = block_height;
		this.tx_input_n = tx_input_n;
		this.tx_output_n = tx_output_n;
		this.confirmations = confirmations;
		this.value = value;
		this.confirmed = confirmed;
		this.received = received;
	}
	
	public DateTime getReceivedOrConfirmed() {
		return confirmed != null ? confirmed : received;
	}

	public String getTx_hash() {
		return tx_hash;
	}

	public void setTx_hash(String tx_hash) {
		this.tx_hash = tx_hash;
	}

	public int getBlock_height() {
		return block_height;
	}

	public void setBlock_height(int block_height) {
		this.block_height = block_height;
	}

	public int getTx_input_n() {
		return tx_input_n;
	}

	public void setTx_input_n(int tx_input_n) {
		this.tx_input_n = tx_input_n;
	}

	public int getTx_output_n() {
		return tx_output_n;
	}

	public void setTx_output_n(int tx_output_n) {
		this.tx_output_n = tx_output_n;
	}

	public int getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(int confirmations) {
		this.confirmations = confirmations;
	}

	public BigInteger getValue() {
		return value;
	}

	public void setValue(BigInteger value) {
		this.value = value;
	}

	public DateTime getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(DateTime confirmed) {
		this.confirmed = confirmed;
	}

	public DateTime getReceived() {
		return received;
	}

	public void setReceived(DateTime received) {
		this.received = received;
	}

	public boolean isSpend() {
		return tx_input_n >= 0;
	}
	
	
}
