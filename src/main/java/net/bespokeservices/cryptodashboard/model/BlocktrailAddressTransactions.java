package net.bespokeservices.cryptodashboard.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlocktrailAddressTransactions {
	protected int current_page;
	protected int per_page;
	protected int total;
	protected ArrayList<BlocktrailTxn> data;
	
	public BlocktrailAddressTransactions() {
		super();
		data = new ArrayList<>();
	}
	
	public int getCurrent_page() {
		return current_page;
	}
	public void setCurrent_page(int current_page) {
		this.current_page = current_page;
	}
	public int getPer_page() {
		return per_page;
	}
	public void setPer_page(int per_page) {
		this.per_page = per_page;
	}
	
	public boolean hasMore() {
		return current_page*per_page < total;
	}
	
	public int getNumPages() {
		int p = total/per_page;
		p += total%per_page > 0 ? 1 : 0;
		return p;
	}

	public ArrayList<BlocktrailTxn> getData() {
		return data;
	}

	public void setData(ArrayList<BlocktrailTxn> data) {
		this.data = data;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	public BlocktrailTxn getFirstTxn() {
		return data == null ?  null : data.get(0);
	}
	
	public BlocktrailTxn getLastTxn() {
		return data == null ?  null : data.get(data.size()-1);
	}
}
