package net.bespokeservices.cryptodashboard.model;

import java.util.ArrayList;
import java.util.Date;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlocktrailTxn {
	protected String hash;
	protected int confirmations;
	protected ArrayList<BlocktrailTxnInputOutput> inputs;
	protected ArrayList<BlocktrailTxnInputOutput> outputs;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	protected DateTime time;

	
	public BlocktrailTxn() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public DateTime getTime() {
		return time;
	}
	
	public void setTime(DateTime time) {
		this.time = time;
	}

	public int getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(int confirmations) {
		this.confirmations = confirmations;
	}

	public ArrayList<BlocktrailTxnInputOutput> getInputs() {
		return inputs;
	}

	public void setInputs(ArrayList<BlocktrailTxnInputOutput> inputs) {
		this.inputs = inputs;
	}

	public ArrayList<BlocktrailTxnInputOutput> getOutputs() {
		return outputs;
	}

	public void setOutputs(ArrayList<BlocktrailTxnInputOutput> outputs) {
		this.outputs = outputs;
	}
	
	
}
