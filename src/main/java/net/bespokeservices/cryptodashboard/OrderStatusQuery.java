package net.bespokeservices.cryptodashboard;

public class OrderStatusQuery {
	public String coinType;
	public String orderType;
	public String orderId;
	private String options;
	
	
	public OrderStatusQuery() {
		super();
		this.options = "";
		// TODO Auto-generated constructor stub
	}


	public String getCoinType() {
		return coinType;
	}


	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}


	public String getOrderType() {
		return orderType;
	}


	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getOptions() {
		return options;
	}


	public void setOptions(String options) {
		this.options = options;
	}
	
	
}
