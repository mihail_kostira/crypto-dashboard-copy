package net.bespokeservices.cryptodashboard;

import org.joda.time.DateTime;

public interface TxnHistoryFetcher {

	void updateTxnHistory(TxnHistory history, DateTime fromDate, DateTime toDate);
}
