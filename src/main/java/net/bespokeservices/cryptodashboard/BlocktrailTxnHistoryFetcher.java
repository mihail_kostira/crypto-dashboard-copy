package net.bespokeservices.cryptodashboard;

import java.io.IOException;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.ReadableInstant;

import net.bespokeservices.cryptodashboard.model.BlocktrailAddressTransactions;
import net.bespokeservices.cryptodashboard.model.BlocktrailTxn;
import net.bespokeservices.cryptodashboard.model.BlocktrailTxnInputOutput;

public class BlocktrailTxnHistoryFetcher implements TxnHistoryFetcher {
	private int txnPerPageLimit;

	private static final Logger logger = Logger.getLogger(BlocktrailTxnHistoryFetcher.class);
		
	public BlocktrailTxnHistoryFetcher() {
		super();
		txnPerPageLimit = 50;
	}

	@Override
	public void updateTxnHistory(TxnHistory history, DateTime fromDate, DateTime toDate) {
		// TODO Auto-generated method stub

		logger.debug("updateTxnHistory " + this);
		try
		{
			int pageNum = 1;
			boolean loadMore = false;

			do {
				BlocktrailAddressTransactions txnsObject;

				txnsObject = loadTxnPage(pageNum, history.getAddress());
				loadMore = processTxns(history, txnsObject, fromDate, toDate);
				pageNum++;
			} while (loadMore);

			history.setLastUpdated(new DateTime());
			history.setUpdateError(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			history.setUpdateError(e.getMessage());
		}

	}

	private BlocktrailAddressTransactions loadTxnPage(int pageNum, CryptoAddress address) throws IOException {
		logger.debug("loadTxnPage,loading page " + pageNum);
		Client client = ClientProvider.getInstance().getClient();
		String apiCallTemplate = "https://api.blocktrail.com/v1/{0}/address/{1}/transactions?api_key={2}&limit={3}&page={4,number,#}&sort_dir=desc";
		Object apiKey = "396d4dc5f9022ba7aefba75fa891d4c125be75fe";

		String coinType = address.getCoinType();
		if  (address.getCoinType().equals("bch")) {
			coinType = "bcc";
		}
		
		String apiCall = MessageFormat.format(apiCallTemplate, 
				coinType, 
				address.getAddress(), 
				apiKey,
				txnPerPageLimit, pageNum);
		BlocktrailAddressTransactions txnsObject;
		
		logger.debug("retrieving " + apiCall);
		
		try {
			txnsObject = client.target(apiCall).request(MediaType.APPLICATION_JSON)
					.get(BlocktrailAddressTransactions.class);
		} catch (Exception e) {
			logger.error("Error retrieving BlocktrailAddressTransactions object at " + apiCall, e);
			throw new IOException("Error retrieving BlocktrailAddressTransactions  at " + apiCall +" Error is: " + e.getMessage(), e);
		}
		return txnsObject;
	}
	
	/**
	 * Adds txns that are in our track time window to the history
	 * 
	 * @param txnsObject
	 * @param fromDate 
	 * @param toDate 
	 * @param history
	 * @return whether to retrieve older pages.False if
	 *  1) the newest txn is before our to-from interval
	 *  or
	 *  2) the oldest txn is already and in our history with >10 confirmations 
	 *  or
	 *  3) there are no more pages
	 */
	private boolean processTxns(TxnHistory history, BlocktrailAddressTransactions txnsObject, 
			ReadableInstant fromDate, ReadableInstant toDate) {
		ArrayList<BlocktrailTxn> txns = txnsObject.getData();
		for (BlocktrailTxn txn : txns) {
			// txns are ordered newest to oldest
			DateTime timeSeen = txn.getTime();
			if (timeSeen.isBefore(fromDate)) {
				// this txn and all older ones are before our track window
				return false;
			}
			if (timeSeen.isAfter(toDate)) {
				// this txn is after our track window, check the earlier ones
				continue;
			}

			logger.debug("in the to-from interval");
			// in the to-from interval
			Txn duplicate = addToHistory(history, txn);
			if (duplicate != null 
					&& duplicate.getConfirmations() > 10) {
				logger.debug("in history and confirmed, skip");
				// we already have this txn in hostory and it has some confirmations
				// so we can be sure earlier txns have been added as well
				return false;
			}
		}

		return txnsObject.hasMore();
	}

	/**
	 * @param btxn
	 * @param history 
	 * @return true if history has a txn with the same hash
	 */
	private Txn addToHistory(TxnHistory history, BlocktrailTxn btxn) {
		logger.debug("addToHistory");
		Txn duplicate = history.getTxn(btxn.getHash());

		if (duplicate != null) {
			duplicate.setConfirmations(btxn.getConfirmations());
			return duplicate;
		}
		
		ArrayList<BlocktrailTxnInputOutput> outputs = btxn.getOutputs();
		for (BlocktrailTxnInputOutput output : outputs) {
			// output.getAddress() is null if e.g., it's pay to script
			if (output.getAddress() != null
					&& output.getAddress().equals(history.getAddress().getAddress())) {
				Txn txn = new Txn(btxn.getHash(), output.getValue(), btxn.getConfirmations(), btxn.getTime(), null);
				logger.debug("adding output " + txn.toString());
				history.addTxn(txn);
			}
		}
		for (BlocktrailTxnInputOutput input : btxn.getInputs()) {
			if (input.getAddress() != null 
					&& input.getAddress().equals(history.getAddress().getAddress())) {
				Txn txn = new Txn(btxn.getHash(), 
						input.getValue().multiply(BigInteger.valueOf(-1)), 
						btxn.getConfirmations(), btxn.getTime(), null);
				logger.debug("adding input " + txn.toString());

				history.addTxn(txn);
			}
		}
		return null;
	}

}
