package net.bespokeservices.cryptodashboard;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

public class TxnHistory {
	private CryptoAddress address;
	private BigInteger valueUnitsPerCoin; // if txn values are quoted in satoshis, this will be 100000000
	private List<Txn> txns;
	private String updateError;
	
	protected DateTime lastUpdated;
	
	public TxnHistory() {
		super();
	}

	public TxnHistory(CryptoAddress address) {
		super();
		this.address = address;
		txns = new ArrayList<Txn>();
	}
	
	public void addTxn(Txn txn) {
		txns.add(txn);
	}
	public List<Txn> getTxns() {
		return txns;
	}
	public void setTxns(List<Txn> list) {
		this.txns = list;
	}
	
	
	
	public String getUpdateError() {
		return updateError;
	}
	public void setUpdateError(String updateError) {
		this.updateError = updateError;
	}
	public DateTime getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(DateTime dateTime) {
		this.lastUpdated = dateTime;
	}
	public boolean hasTxn(String hash) {
		for (Txn txn : txns) {
			if (txn.getHash().equals(hash)) {
				return true;
			}
		}
		return false;
	}
	public Txn getTxn(String hash) {
		for (Txn txn : txns) {
			if (txn.getHash().equals(hash)) {
				return txn;
			}
		}
		return null;
	}
	public CryptoAddress getAddress() {
		return address;
	}

	public BigInteger getValueUnitsPerCoin() {
		return valueUnitsPerCoin;
	}

	public void setValueUnitsPerCoin(BigInteger valueUnitsPerCoin2) {
		this.valueUnitsPerCoin = valueUnitsPerCoin2;
	}

	@Override
	public String toString() {
		return "TxnHistory [address=" + address + ", valueUnitsPerCoin=" + valueUnitsPerCoin + ", txns=" + txns
				+ ", updateError=" + updateError + ", lastUpdated=" + lastUpdated + "]";
	}

	/**
	 * 
	 * @return a string that omits lastUpdated. It stays the same regardless of when the history was retrieved
	 */
	public String toTestableString() {
		return "TxnHistory [address=" + address + ", valueUnitsPerCoin=" + valueUnitsPerCoin + ", txns=" + txns
				+ ", updateError=" + updateError + "]";
	}
	
	public void remove(Txn duplicate) {
		txns.remove(duplicate);
	}
	
	
	
	
}
