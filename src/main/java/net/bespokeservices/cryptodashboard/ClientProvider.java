package net.bespokeservices.cryptodashboard;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientProperties;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class ClientProvider {

	private static ClientProvider instance = null;
	private Client client;

	public static ClientProvider getInstance() {
		if (instance == null) {
			instance = new ClientProvider();
		}
		return instance;
	}

	protected ClientProvider() {
		super();
		JacksonJsonProvider jacksonJsonProvider = new JacksonJaxbJsonProvider();
		JodaMapper mapper = new JodaMapper();
		jacksonJsonProvider.setMapper(mapper);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		client = ClientBuilder.newClient().register(jacksonJsonProvider);
		client.property(ClientProperties.CONNECT_TIMEOUT, 6000);
		client.property(ClientProperties.READ_TIMEOUT, 9000);
	}

	public Client getClient() {
		return client;
	}
}
