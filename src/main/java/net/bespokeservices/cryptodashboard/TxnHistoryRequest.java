package net.bespokeservices.cryptodashboard;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TxnHistoryRequest {
	private String coinType;
	private String address;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	protected DateTime fromDate;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	protected DateTime toDate;
	
	private boolean track;
	private long cacheMaxAgeSecs;
	
	public TxnHistoryRequest() {
		super();
	}
	
	public TxnHistoryRequest(String coinType, String address, DateTime fromDate, DateTime toDate, boolean track,
			long cacheMaxAgeSecs) {
		super();
		this.coinType = coinType;
		this.address = address;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.track = track;
		this.cacheMaxAgeSecs = cacheMaxAgeSecs;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public DateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(DateTime fromDate) {
		this.fromDate = fromDate;
	}

	public DateTime getToDate() {
		return toDate;
	}

	public void setToDate(DateTime toDate) {
		this.toDate = toDate;
	}

	public boolean isTrack() {
		return track;
	}

	public void setTrack(boolean track) {
		this.track = track;
	}

	public long getCacheMaxAgeSecs() {
		return cacheMaxAgeSecs;
	}

	public void setCacheMaxAgeSecs(long cacheMaxAgeSecs) {
		this.cacheMaxAgeSecs = cacheMaxAgeSecs;
	}

	@Override
	public String toString() {
		return "TxnHistoryRequest [coinType=" + coinType + ", address=" + address + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + ", track=" + track + ", cacheMaxAgeSecs=" + cacheMaxAgeSecs + "]";
	}
}
