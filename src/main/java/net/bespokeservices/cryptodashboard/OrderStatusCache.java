package net.bespokeservices.cryptodashboard;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

public class OrderStatusCache {
	private Map<String, OrderStatus> statusCache;
	private int maxAgeSeconds;

	public OrderStatusCache() {
		this(60);
	}
	
	public OrderStatusCache(int maxAgeSeconds) {
		super();
		this.maxAgeSeconds = maxAgeSeconds;
		statusCache = new HashMap<String, OrderStatus>();
	}

	public Map<String, OrderStatus> getStatusCache() {
		return statusCache;
	}

	public OrderStatus getStatus(String orderId) {
		OrderStatus status = statusCache.get(orderId);
		if (status != null 
				&& status.getLastUpdated().plusSeconds(maxAgeSeconds).isAfterNow()) {
			return status;
		}
		return null;
	}

	public void putStatus(String orderId, OrderStatus status) {
		statusCache.put(orderId, status);
		
	}
	
	
		
}
