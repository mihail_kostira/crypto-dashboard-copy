package net.bespokeservices.cryptodashboard;

public class CryptoAddress {
	private String coinType;
	private String address;
	

	public CryptoAddress() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CryptoAddress(String coinType, String address) {
		super();
		this.coinType = coinType;
		this.address = address;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((coinType == null) ? 0 : coinType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CryptoAddress other = (CryptoAddress) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (coinType == null) {
			if (other.coinType != null)
				return false;
		} else if (!coinType.equals(other.coinType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CryptoAddress [coinType=" + coinType + ", address=" + address + "]";
	}
	
	
 
}
