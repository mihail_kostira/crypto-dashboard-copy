package net.bespokeservices.cryptodashboard;

import java.math.BigInteger;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

public class TxnHistoryService {
	
	private static final Logger logger = Logger.getLogger(TxnHistoryService.class);
	
	private HashMap<CryptoAddress, TxnHistory> histories;
	private TxnFetcherFactory fetcherFactory;
	
	public TxnHistoryService() {
		super();
		histories = new HashMap<CryptoAddress, TxnHistory>();
	}

	public synchronized TxnHistory getTxnHistory(TxnHistoryRequest request) {
		logger.debug("getTxnHistory " + request);
		CryptoAddress address = new CryptoAddress(request.getCoinType(), request.getAddress());
		TxnHistory history = histories.get(address);
		if (history == null) {
			logger.debug("History for address not found, creating");
			history = new TxnHistory(address);
			switch (request.getCoinType()) {
			case "btc":
				history.setValueUnitsPerCoin(new BigInteger("100000000"));
				break;
			case "bch":
				history.setValueUnitsPerCoin(new BigInteger("100000000"));
				break;
			case "eth":
				history.setValueUnitsPerCoin(new BigInteger("1000000000000000000"));
				break;
			case "doge":
				history.setValueUnitsPerCoin(new BigInteger("100000000"));
				break;
			case "ltc":
				history.setValueUnitsPerCoin(new BigInteger("100000000"));
				break;
			case "dash":
				history.setValueUnitsPerCoin(new BigInteger("100000000"));
				break;
			}
			histories.put(address, history);
		}
		// TODO setup tracking if needed
		if (!isHistoryUpToDate(history, request)) {
			logger.debug("History not up to date, updating");
			TxnHistoryFetcher fetcher = fetcherFactory.getFetcher(address);
			fetcher.updateTxnHistory(history, request.getFromDate(), request.getToDate());
		}
		logger.debug("returning " + history);
		return history;
	}

	private boolean isHistoryUpToDate(TxnHistory history, TxnHistoryRequest request) {
		DateTime lastUpdated = history.getLastUpdated();
		if (lastUpdated == null) {
			return false;
		}
		
		if (lastUpdated.isAfter(request.getToDate())) {
			return true;
		}
		
		if (lastUpdated.plusSeconds((int) request.getCacheMaxAgeSecs()).isAfterNow()) {
			return true;
		}
		
		return false;
	}

	public void setFetcherFactory(TxnFetcherFactory fetcherFactory) {
		this.fetcherFactory = fetcherFactory;
	}

}
