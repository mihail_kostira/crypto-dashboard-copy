package net.bespokeservices.cryptodashboard.server;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.security.ConstraintAware;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.SecurityHandler;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.HandlerWrapper;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.resource.FileResource;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class EmbeddedServer implements ApplicationContextAware {
	private static final Logger logger = Logger.getLogger(EmbeddedServer.class);
	private int httpPort;
	private int httpsPort;
	private boolean forceHTTPS;
	private String sslKeystorePassword;
	private String sslKeystorePath;
	private String sslKeyPassword;
	private Server jettyServer;
	private AnnotationConfigWebApplicationContext webApplicationContext;

	/**
	 * Note! The parentApplicationContext field must be set before the server can be
	 * started
	 * 
	 * @param httpPort
	 * @param httpsPort
	 * @param forceHttps
	 * @param sslKeystorePath
	 * @param sslKeystorePassword
	 * @param sslKeyPassword
	 */
	public EmbeddedServer(Integer httpPort, Integer httpsPort, boolean forceHttps, String sslKeystorePath,
			String sslKeystorePassword, String sslKeyPassword) {
		this.httpPort = httpPort;
		this.httpsPort = httpsPort;
		this.forceHTTPS = forceHttps;
		this.sslKeystorePath = sslKeystorePath;
		this.sslKeystorePassword = sslKeystorePassword;
		this.sslKeyPassword = sslKeyPassword;
	}

	@Override
	public void setApplicationContext(ApplicationContext parentApplicationContext) throws BeansException {
		webApplicationContext = new AnnotationConfigWebApplicationContext();
		webApplicationContext.setParent(parentApplicationContext);
		// tell webApplicationContext to look for annotated classes here
		webApplicationContext.setConfigLocation("net.bespokeservices.cryptodashboard.server");
		webApplicationContext.refresh();
	}

	/**
	 * This method must be called AFTER the parent ApplicationContext is created;
	 * otherwise requests to injected Jersey resources hang for some reason
	 * (the reason likely being that the child ApplicationContext is trying to 
	 * do stuff before the parent is constructed)
	 *  
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public void start() throws IOException, URISyntaxException {
		logger.error("starting " + this);

		if (webApplicationContext == null) {
			throw new IllegalStateException("Parent application context not set");
		}

		jettyServer = new Server();

		// Setup HTTP Connector
		HttpConfiguration httpConf = new HttpConfiguration();
		httpConf.setSecurePort(httpsPort);
		httpConf.setSecureScheme("https");
		// Establish the HTTP ServerConnector
		ServerConnector httpConnector = new ServerConnector(jettyServer, new HttpConnectionFactory(httpConf));
		httpConnector.setPort(httpPort);

		// Setup SslContextFactory
		URL keystoreUrl = Thread.currentThread().getContextClassLoader().getResource(sslKeystorePath);
		String keyStorePath = keystoreUrl.getPath();
		SslContextFactory sslContextFactory = new SslContextFactory();
		sslContextFactory.setKeyStorePath(keyStorePath);
		sslContextFactory.setKeyStorePassword(sslKeystorePassword);
		sslContextFactory.setKeyManagerPassword(sslKeyPassword);
		// Setup HTTPS Configuration
		HttpConfiguration httpsConf = new HttpConfiguration();
		httpsConf.setSecurePort(httpsPort);
		httpsConf.setSecureScheme("https");
		httpsConf.addCustomizer(new SecureRequestCustomizer()); // adds ssl info to request object
		// Establish the HTTPS ServerConnector
		ServerConnector httpsConnector = new ServerConnector(jettyServer,
				new SslConnectionFactory(sslContextFactory, "http/1.1"), new HttpConnectionFactory(httpsConf));
		httpsConnector.setPort(httpsPort);

		jettyServer.setConnectors(new Connector[] { httpConnector, httpsConnector });

		Handler handler = buildServerHandlers();
		jettyServer.setHandler(handler);

		try {
			logger.info("starting jetty server");
			jettyServer.start();
			jettyServer.join();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			jettyServer.destroy();
		}
	}

	private Handler buildServerHandlers() throws IOException, URISyntaxException {
		HandlerList handlers = new HandlerList();

		ServletContextHandler jerseyServletContexttHandler = buildJerseyServletHandler("/rest/");
		handlers.addHandler(jerseyServletContexttHandler);

		ContextHandler websocketServletContexttHandler = buildWebsocketServletHandler("/ws");
		handlers.addHandler(websocketServletContexttHandler);

		ResourceHandler resourceHandler = buildStaticResourceHandler();
		handlers.addHandler(resourceHandler);

		handlers.addHandler(new DefaultHandler());

		if (!forceHTTPS) {
			return handlers;
		} else {
			ConstraintSecurityHandler securityHandler = buildConstraintSecurityHandler();
			securityHandler.setHandler(handlers);

			return securityHandler;
		}
	}

	private ContextHandler buildWebsocketServletHandler(String contextPath) {
		WebSocketHandler wsHandler = new WebSocketHandler() {
			@Override
			public void configure(WebSocketServletFactory factory) {
				factory.register(EventSocket.class);
			}
		};
		ContextHandler context = new ContextHandler();
		context.setContextPath(contextPath);
		context.setHandler(wsHandler);
		return context;
	}

	private ServletContextHandler buildJerseyServletHandler(String contextPath) {
		// now create a handler to serve Jersey resources, use the
		// parentApplicationContext
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath(contextPath);
		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);
		// Tells the Jersey Servlet where to find resources & providers 
		// (e.g. serialization helpers)
		jerseyServlet.setInitParameter("jersey.config.server.provider.packages",
				"net.bespokeservices.cryptodashboard.server.resources, "+
				"com.fasterxml.jackson.jaxrs.json, "+
				"net.bespokeservices.jerseyproviders");
				//"net.bespokeservices.model");
		// we want jackson to deserialize our JSON, so disable moxy
		jerseyServlet.setInitParameter("jersey.config.server.disableMoxyJson",
				"true");
		
		// Add listeners to enable Spring dependency injection in Jersey resource
		// classes;
		// We're using a web application context that is a child of the global Spring
		// context
		context.addEventListener(new ContextLoaderListener(webApplicationContext));
		context.addEventListener(new RequestContextListener());

		// allow cross origin requests, e.g. ajax calls from a different domain
		FilterHolder cors = context.addFilter(CrossOriginFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
		cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,HEAD");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin");

		return context;

	}

	private ResourceHandler buildStaticResourceHandler() throws IOException, URISyntaxException {
		// create a ResourceHandler for serving static files..
		ResourceHandler resourceHandler = new ResourceHandler();
		resourceHandler.setDirectoriesListed(true);
		// contain static resource serving to htdocs
		URL url = Thread.currentThread().getContextClassLoader().getResource("resources-external/htdocs");
		Resource resource;
		resource = new FileResource(url);
		resourceHandler.setBaseResource(resource);
		return resourceHandler;
	}

	private ConstraintSecurityHandler buildConstraintSecurityHandler() {
		// Setup security constraint and a security handler
		Constraint constraint = new Constraint();
		constraint.setDataConstraint(Constraint.DC_CONFIDENTIAL);
		ConstraintMapping mapping = new ConstraintMapping();
		mapping.setPathSpec("/*");
		mapping.setConstraint(constraint);
		ConstraintSecurityHandler securityHandler = new ConstraintSecurityHandler();
		securityHandler.addConstraintMapping(mapping);
		return securityHandler;
	}

	public boolean isStarted() {
		return jettyServer != null && jettyServer.isStarted();
	}

	public void stop() throws Exception {
		if (jettyServer == null) {
			throw new IllegalStateException();
		}
		jettyServer.stop();
	}
}
