package net.bespokeservices.cryptodashboard.server;

public class EmbeddedServerAdminSettings {
	protected boolean enableAdminAcess;
	protected String adminPassword;
	
	public EmbeddedServerAdminSettings(boolean enableAdminAcess, String adminPassword) {
		super();
		this.enableAdminAcess = enableAdminAcess;
		this.adminPassword = adminPassword;
	}

	public boolean isEnableAdminAcess() {
		return enableAdminAcess;
	}

	public String getAdminPassword() {
		return adminPassword;
	}
	
	
}
