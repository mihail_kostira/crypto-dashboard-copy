package net.bespokeservices.cryptodashboard.server.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import com.mchange.v2.resourcepool.TimeoutException;

import net.bespokeservices.cryptodashboard.CrawlerParseException;
import net.bespokeservices.cryptodashboard.OrderStatus;
import net.bespokeservices.cryptodashboard.OrderStatusCache;
import net.bespokeservices.cryptodashboard.OrderStatusChecker;
import net.bespokeservices.cryptodashboard.OrderStatusQuery;
import net.bespokeservices.cryptodashboard.WebDriverPool;

@Path("/orderstatus")
public class OrderStatusResource {
	private static final Logger logger = Logger.getLogger(OrderStatusResource.class);

	@Autowired
	public OrderStatusChecker orderStatusChecker;
	
	@Autowired
	public OrderStatusCache orderStatusCache;
	 
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public OrderStatus getOrderStatus(OrderStatusQuery query) {
    	OrderStatus status;
    	 
		status = orderStatusCache.getStatus(query.orderId);
    	if (status == null
    		|| query.getOptions().contains("forceUpdate")) {
    		try {
				status = checkOrderStatus(query.coinType, query.orderType, query.orderId);
			} catch (Exception e) {
				logger.error("error checking order status: " + e.getMessage(), e);
				throw new WebApplicationException(
						Response.serverError().entity("error checking order status: " + e.getMessage()).type("text/plain").build());
			}
    		orderStatusCache.putStatus(query.orderId, status);
    	} else {
    		logger.info("returning cached status");
    	}
    	
    	return status;
    }
    public OrderStatus checkOrderStatus(String coinType, String orderType, String orderId) throws Exception {
		OrderStatus status = null;
		status = orderStatusChecker.checkOrderStatus(coinType, orderType, orderId);
		
		return status;
	}
}