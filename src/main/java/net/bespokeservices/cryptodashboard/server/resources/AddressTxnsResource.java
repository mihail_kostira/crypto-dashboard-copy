package net.bespokeservices.cryptodashboard.server.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mchange.v2.resourcepool.TimeoutException;

import net.bespokeservices.cryptodashboard.OrderStatus;
import net.bespokeservices.cryptodashboard.OrderStatusQuery;
import net.bespokeservices.cryptodashboard.TxnHistory;
import net.bespokeservices.cryptodashboard.TxnHistoryRequest;
import net.bespokeservices.cryptodashboard.TxnHistoryService;
import net.bespokeservices.cryptodashboard.server.EmbeddedServerAdminSettings;

@Path("/txns")
public class AddressTxnsResource {
	
	private static final Logger logger = Logger.getLogger(AddressTxnsResource.class);
	
	@Autowired
	public TxnHistoryService txnHistoryService;

	/**
	 * @param request
	 * @return a TxnHistory. NOTE: the number of confirmations for txns in the history 
	 * is NOT kept up to date. The real number of confirmations is at least the reported one.
	 * 
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public TxnHistory getAddressTxnHistory(TxnHistoryRequest request) {
		TxnHistory txnHistory;
		try {
			txnHistory = txnHistoryService.getTxnHistory(request);
		} catch (Exception e) {
			logger.error("error getting txn history: " + e.getMessage(), e);
			throw new WebApplicationException(
					Response.serverError().entity("error getting txn history: " + e.getMessage() + " " + ExceptionUtils.getStackTrace(e)).type("text/plain").build());
		}
		
		return txnHistory;
    }
}
