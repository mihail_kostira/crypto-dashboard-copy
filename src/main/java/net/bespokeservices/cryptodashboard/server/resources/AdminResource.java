package net.bespokeservices.cryptodashboard.server.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import net.bespokeservices.cryptodashboard.App;
import net.bespokeservices.cryptodashboard.OrderStatus;
import net.bespokeservices.cryptodashboard.OrderStatusCache;
import net.bespokeservices.cryptodashboard.OrderStatusChecker;
import net.bespokeservices.cryptodashboard.server.EmbeddedServerAdminSettings;


@Path("/admin")
public class AdminResource {
	
	private static final Logger logger = Logger.getLogger(AdminResource.class);
    
	@Autowired
	public EmbeddedServerAdminSettings serverAdminSettings;
		
	@GET
    @Path("/terminate")
    public void terminate(@QueryParam("pwd") String restAPIAdminPassword
    		) {
    	if (!serverAdminSettings.isEnableAdminAcess()) {
    		logger.error("ignoring terminate rquest, restAPIAdminAccessEnabled is false");
    		return;
    	}
    	if (restAPIAdminPassword != null &&
    			restAPIAdminPassword.equals(serverAdminSettings.getAdminPassword())) {
    		logger.error(" terminate rquest, shutting down");
    		System.exit(0);
    	}
    }
}
