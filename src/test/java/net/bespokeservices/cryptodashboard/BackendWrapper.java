package net.bespokeservices.cryptodashboard;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * A wrapper around the backend for testing
 */
public class BackendWrapper {

	private static final Logger logger = Logger.getLogger(BackendWrapper.class);

	private static CryptoDasboardBackend backend;
	
	public BackendWrapper() {
		super();
	}

	public void configure() throws IOException {
		logger.debug("Configuring testing backend, loading config file..");
		// Load config file as props
		Properties config = new Properties();
		String configFilePath = "resources-external/cryptodashboard-test.cfg";
		try {
			InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFilePath);
			config.load(stream);
		} catch (IOException e1) {
			System.err.println("Error loading config file from" + configFilePath);
			throw (e1);
		}
		// start the server
		backend = new CryptoDasboardBackend(config);
	}

	/**
	 * Starts the backend in a new thread and waits until it is started before returning
	 */
	public void start() {
		logger.debug("Starting testing backend..");

		new Thread(new Runnable() {
			@Override
			public void run() {
				backend.start();
			}
		}).start();

		while (!backend.isStarted()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		logger.debug("Testing backend started succesfully");
	}
	
	

	public void stop() throws Exception {
		logger.debug("Stopping testing backend..");
		backend.stop();
	}

}
