package net.bespokeservices.cryptodashboard;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

public class PredefinedTxnHistoryRequests {
	private List<TxnHistoryRequest> requests;
	private TxnHistoryRequest wrongAddressRequest;

	public PredefinedTxnHistoryRequests() {
		super();
		
		requests = new ArrayList<TxnHistoryRequest>();
		
		String address;
		String coinType;
		DateTime fromDate;
		DateTime toDate;
		boolean track;
		long cacheMaxAgeSecs;
		
		track = false;
		cacheMaxAgeSecs = 60;
		
		coinType = "bch";
		address = "wrong address 1BGZsS7HE1c4PPKrZ2PjmSywYpwqhwCE48";
		fromDate = new DateTime("2018-03-01T00:00:00+00:00");
		toDate = new DateTime("2018-03-12T00:00:00+00:00");
		wrongAddressRequest = constructRequest(coinType, address, fromDate, toDate, track, cacheMaxAgeSecs);
		
		coinType = "bch";
		address = "1MBvoDsfG6jCTnuNk44CKZEDpcBkybbJMy";
		fromDate = new DateTime("2018-03-01T00:00:00+00:00");
		toDate = new DateTime("2018-03-12T00:00:00+00:00");
		requests.add(constructRequest(coinType, address, fromDate, toDate, track, cacheMaxAgeSecs));
		
		coinType = "eth";
		address = "0xB6D1A0614631C1E5c0f56567656A3872E0B4456b";
		fromDate = new DateTime("2017-10-20T00:00:00+00:00");
		toDate = new DateTime("2018-02-20T00:00:00+00:00");
		requests.add(constructRequest(coinType, address, fromDate, toDate, track, cacheMaxAgeSecs));
		
		coinType = "btc";
		address = "1H6ZZpRmMnrw8ytepV3BYwMjYYnEkWDqVP";
		fromDate = new DateTime("2018-02-27T00:00:00+00:00");
		toDate = new DateTime("2018-03-01T00:00:00+00:00");
		requests.add(constructRequest(coinType, address, fromDate, toDate, track, cacheMaxAgeSecs));
		
	}

	private TxnHistoryRequest constructRequest(String coinType, String address, DateTime fromDate, DateTime toDate, boolean track,
			long cacheMaxAgeSecs) {
		
		TxnHistoryRequest request = new TxnHistoryRequest(coinType, address, fromDate, toDate, track, cacheMaxAgeSecs);
		return request;
	}

	public List<TxnHistoryRequest> getRequests() {
		return requests;
	}

	public TxnHistoryRequest getWrongAddressRequest() {
		return wrongAddressRequest;
	}
	
	
}
