
package net.bespokeservices.cryptodashboard;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientProperties;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class TestingClientProvider {

	private static TestingClientProvider instance = null;
	private Client client;

	public static TestingClientProvider getInstance() {
		if (instance == null) {
			instance = new TestingClientProvider();
		}
		return instance;
	}

	protected TestingClientProvider() {
		super();
		JacksonJsonProvider jacksonJsonProvider = new JacksonJaxbJsonProvider();
		JodaMapper mapper = new JodaMapper();
		jacksonJsonProvider.setMapper(mapper);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		client = ClientBuilder.newClient().register(jacksonJsonProvider);
		client.property(ClientProperties.CONNECT_TIMEOUT, 5000);
		client.property(ClientProperties.READ_TIMEOUT, 30000);
	}

	public Client getClient() {
		return client;
	}
}
