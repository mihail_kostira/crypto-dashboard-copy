package net.bespokeservices.cryptodashboard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientProperties;
import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;

import net.bespokeservices.cryptodashboard.model.BlocktrailAddressTransactions;

public class TxnHistoryFetchingTest {

	private static final Logger logger = LogManager.getLogger(TxnHistoryFetchingTest.class);

	private static BackendWrapper backend;

	private static String txnsApiUrl = "http://127.0.0.1/rest/txns";

	@BeforeClass
	public static void setUpClass() throws IOException, InterruptedException {
		logger.error("Setting up server to run tests against.");
		logger.error("Tests depend on getting data from external APIs (blocktrail, blockcypher). Tests will fail if there's no connectivity, APIs are down or have changed.");
		
		backend = new BackendWrapper();
		backend.configure();
		backend.start();
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
		backend.stop();
	}
	
	@Test
	public void shouldReturnNonNullTxnHistory() {
		logger.debug("shouldReturnNonNullTxnHistory");
		
		List<TxnHistoryRequest> requests = new PredefinedTxnHistoryRequests().getRequests();

		TxnHistoryRequest request = requests.get(0);
		TxnHistory txnHistory = makeRequest(request);

		logger.debug("txnHistory" + txnHistory);

		assertNotNull(txnHistory);
	}
	
	@Test
	public void shouldReturnTxnHistoryWithErrorGivenWrongAddress() {
		logger.debug("shouldReturnTxnHistoryWithErrorGivenWrongAddress");
		
		TxnHistoryRequest request = new PredefinedTxnHistoryRequests().getWrongAddressRequest();
		TxnHistory txnHistory = makeRequest(request);

		logger.debug("txnHistory" + txnHistory);

		assertNotNull(txnHistory);
		assertNotNull(txnHistory.getUpdateError());
	}	
	
	@Test
	public void shouldMatchPreviouslyRetrievedKnownGoodOutput() throws IOException {
		logger.debug("shouldMatchPreviouslyRetrievedKnownGoodOutput");
		
		URL referenceFileUrl = Thread.currentThread().getContextClassLoader().getResource("resources-external/txn-histories-known-good-output.txt");
		//byte[] encoded = Files.readAllBytes(Paths.get(referenceFileUrl.getPath()));
		//byte[] encoded = Files.readAllBytes(Paths.get("./resources-external/txn-histories-known-good-output.txt"));
		//File file = new File(referenceFileUrl.getFile());
		//String path = file.getPath();
		byte[] encoded = Files.readAllBytes(Paths.get(new File(referenceFileUrl.getFile()).getPath()));
		
		String reference = new String(encoded, StandardCharsets.UTF_8);
		
		List<TxnHistoryRequest> requests = new PredefinedTxnHistoryRequests().getRequests();

		StringBuilder output = new StringBuilder();
		
		for (TxnHistoryRequest request : requests) {
			TxnHistory txnHistory = makeRequest(request);
			output.append(txnHistory.toTestableString());
			//output.append("");
		}
		
		logger.debug("--------output: \n" + output + "\n------");
		
		assertNotNull(output);
		assertTrue(output.toString().equals(reference));
	}

	private TxnHistory makeRequest(TxnHistoryRequest request) {
		TxnHistory txnHistory = TestingClientProvider.getInstance().getClient()
				.target(txnsApiUrl).request(MediaType.APPLICATION_JSON)
				//.property(ClientProperties.CONNECT_TIMEOUT, 500)
				//.property(ClientProperties.READ_TIMEOUT, 50000)
				.post(Entity.json(request),
				TxnHistory.class);
		return txnHistory;
	}
}
